from google.cloud import bigquery
from datetime import datetime

# Define the list of tables to remove from all datasets
TABLES_TO_REMOVE = ["ng_report_skill", "ng_report_user", "ng_report_agent_group"]
# Define ProjectId to run the deletion
PROJECT_ID = "int2-us-1-6683" 

def remove_tables(project_id, datasets, tables_to_remove):
    client = bigquery.Client(project=project_id)
    datasets = client.list_datasets()
    for dataset in datasets:
        dataset_ref = client.dataset(dataset.dataset_id)
        for table_name in tables_to_remove:
            table_ref = dataset_ref.table(table_name)
            try:
                client.delete_table(table_ref)
                print_with_time(f"Deleted table {table_name} in dataset {dataset.dataset_id} in project {project_id}")
            except Exception as e:
                # Handle exceptions that is not a NotFound
                if 'Not found' not in str(e):
                    print(f"An error occurred: {str(e)}")
                
                
            

def list_bigquery_datasets(project_id):
    print_with_time("Getting Datasets to delete from BigQuery Datasets...")
    client = bigquery.Client(project=project_id)
    datasets = client.list_datasets()  # Retrieves an iterator of dataset reference objects
    dataset_names = [dataset.dataset_id for dataset in datasets]  # Extracts dataset IDs
    return dataset_names

def print_with_time(msg):
    current_time_str = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    print("[{}] {}".format(current_time_str, msg))

if __name__ == "__main__":
    print_with_time(f"Executing for:\nProject ID -> {PROJECT_ID}\nTables to remove from all datasets -> {TABLES_TO_REMOVE}")

    bigquery_datasets = list_bigquery_datasets(PROJECT_ID)
    print_with_time(f"Datasets in project {PROJECT_ID} that will be checked for possible clean: {bigquery_datasets}")

    remove_tables(PROJECT_ID, bigquery_datasets, TABLES_TO_REMOVE)
