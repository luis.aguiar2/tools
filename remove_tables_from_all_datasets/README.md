# BigQuery Tables Cleaning Script

This script automates the cleaning of a specified list of tables across all datasets within a Google Cloud Platform (GCP) Project for BigQuery.

## Constants

Ensure you set the following constants in the `.py` file:

TABLES_TO_REMOVE = [...]  # List of tables to remove from all datasets
PROJECT_ID = "your-project-id"

## Google Cloud SDK Setup

Before running the script locally or within a Docker container, make sure you have the Google Cloud SDK installed and configured with the necessary credentials.

## Running with Docker

You can run the script using Docker. Follow these steps:

1. Build the Docker image:

docker build -t remove_tables_from_all_datasets .


2. Run the Docker container:

docker run -v "$HOME/.config/gcloud/application_default_credentials.json":/gcp/creds.json:ro --env GOOGLE_APPLICATION_CREDENTIALS=/gcp/creds.json remove_tables_from_all_datasets


## Running Locally

To run the script locally, ensure you have Python installed, along with the following dependency:

- `google-cloud-bigquery` (install via pip)

After installing the dependency, run the script using:

python3 remove_tables_from_all_datasets.py