# Tools Repository

This repository contains useful scripts to automate recurrent tasks

| Tool Name                      | Description                                                                                      |
|----------------------------------|--------------------------------------------------------------------------------------------------|
| remove_kinds_from_all_namespaces | Cleans specified kinds from Datastore in the entire GCP project specified.                                 |
| remove_tables_from_all_datasets  | Cleans specified tables from BigQuery in the entire GCP project specified.                                  |

