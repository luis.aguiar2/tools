from google.cloud import datastore, bigquery
from datetime import datetime

# Define the list of kinds to remove
KINDS_TO_REMOVE = ["ng_report__users", "ng_report__skills", "ng_report__agent_groups", "ng_report__dimension_migration"]
# Define ProjectId to run the deletion
PROJECT_ID = "int2-us-1-6683"  

def remove_kinds_from_namespaces(project_id, namespaces, kinds_to_remove):
    client = datastore.Client(project_id)
    for namespace in namespaces:
        client.namespace = namespace
        for kind in kinds_to_remove:
            query = client.query(kind=kind)
            entities = list(query.fetch())
            for entity in entities:
                client.delete(entity.key)
                print_with_time(f"Deleted entity with key {entity.key} in namespace {namespace} and kind {kind}")

def list_bigquery_datasets(project_id):
    print_with_time("Getting Kinds to delete from BigQuery Datasets...")
    client = bigquery.Client(project=project_id)
    datasets = client.list_datasets()  # Retrieves an iterator of dataset reference objects
    dataset_names = [dataset.dataset_id for dataset in datasets]  # Extracts dataset IDs
    return dataset_names
    

def print_with_time(msg):
    current_time_str = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    print("[{}] {}".format(current_time_str, msg))


if __name__ == "__main__":
    print_with_time(f"Executing for:\nProject ID -> {PROJECT_ID}\nKinds to remove -> {KINDS_TO_REMOVE}")
    bigquery_datasets = list_bigquery_datasets(PROJECT_ID)
    print_with_time(f"Kinds in project {PROJECT_ID} that will be checked for possible clean: {bigquery_datasets}")

    remove_kinds_from_namespaces(PROJECT_ID, bigquery_datasets, KINDS_TO_REMOVE)