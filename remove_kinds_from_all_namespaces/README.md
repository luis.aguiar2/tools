# Datastore Kinds Cleaning Script

This script automates the cleaning of specified kinds across all namespaces within a Google Cloud Platform (GCP) Project for Datastore.

## Constants

Ensure you set the following constants in the `.py` file:

KINDS_TO_REMOVE = [...]  # List of kinds to remove from all namespaces
PROJECT_ID = "your-project-id"

## Google Cloud SDK Setup

Before running the script locally or within a Docker container, make sure you have the Google Cloud SDK installed and configured with the necessary credentials.

## Running with Docker

You can run the script using Docker. Follow these steps:

1. Build the Docker image:

docker build -t remove_kinds_from_all_namespaces .


2. Run the Docker container:

docker run -v "$HOME/.config/gcloud/application_default_credentials.json":/gcp/creds.json:ro --env GOOGLE_APPLICATION_CREDENTIALS=/gcp/creds.json remove_kinds_from_all_namespaces


## Running Locally

To run the script locally, ensure you have Python installed, along with the following dependencies:

- `google-cloud-datastore` (install via pip)
- `google-cloud-bigquery` (install via pip)

After installing the dependencies, run the script using:

python3 remove_kinds_from_all_namespaces.py